
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh 2>/dev/null
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh 2>/dev/null
source ~/.bin/shortcuts/shortcuts
source ~/.bin/shortcuts/commands
ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE="fg=#ff00ff,bg=magenta,bold,underline"

zstyle ':completion:*' menu select
#zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # case-insensitive matching

zmodload zsh/complist

setopt SHARE_HISTORY
setopt HIST_IGNORE_SPACE
setopt HIST_IGNORE_ALL_DUPS
setopt autocd
setopt COMPLETE_ALIASES
setopt completealiases
stty stop undef
export MAKEFLAGS="-j12 -l11"
export KEYTIMEOUT=1
autoload -Uz compinit promptinit edit-command-line colors
_comp_options+=(globdots)
typeset -U PATH path


autoload -Uz vcs_info
alias clear='NEW_LINE=false && clear' # no preceeding newline after clear
zstyle ':vcs_info:git:*' formats '%b '

compinit
promptinit
zle -N edit-command-line zle-line-finish

HISTFILE=~/.cache/zsh/history
HISTSIZE=10000
SAVEHIST=10000
# don't add garbage to history
function hist() {
    [[ "$#1" -lt 7 \
    || "$1" = "run-help "* \
    || "$1" = "cd "* \
    || "$1" = "man "* \
	|| "$1" = "h "* \
    || "$1" = "~ "* ]]
    return $(( 1 - $? ))
}

plugins=(zsh-completions zsh-autosuggestions zsh-highlighting)
REPORTTIME=15

# key bindings
bindkey "\e[7~" beginning-of-line # urxvt
bindkey "\e[8~" end-of-line # urxvt
bindkey "\e[3~" delete-char
bindkey '^[[A' up-line-or-search
bindkey '^[[B' down-line-or-search
bindkey -v
bindkey '^e' edit-command-line

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

PROMPT="%B[%F{magenta}%n%F{yellow}@%F{cyan}%M %F{white}%~]%F{green}$ %f%b${vcs_info_msg_0_}"
echo -ne '\e[5 q' # Use beam shape cursor on startup.

path=(~/.local/.bin ~/.cargo/bin /home/hexdsl/.gem/ruby/2.6.0/bin  ~/.local/bin $path[@])
PATH="/home/hexdsl/perl5/bin${PATH:+:${PATH}}"; export PATH;
PERL5LIB="/home/hexdsl/perl5/lib/perl5${PERL5LIB:+:${PERL5LIB}}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="/home/hexdsl/perl5${PERL_LOCAL_LIB_ROOT:+:${PERL_LOCAL_LIB_ROOT}}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"/home/hexdsl/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=/home/hexdsl/perl5"; export PERL_MM_OPT;
