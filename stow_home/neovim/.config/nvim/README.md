# VIM CHEATSHEET

Most of this stuff is standard VIM commands. Scroll down a little way and
there's some stuff about Plugins. This is a work in progress and will change a
lot. Even though this is a markdown file I feel like its best viewed directly
in vim. It is formatted nicer.

***The last section is my custom settings***

--------

# Standard VIM stuff

## CURSOR MOVEMENT

* h                   move left
* j                   move down
* k                   move up
* l                   move right
* w                   jump by start of words (punctuation considered words)
* W                   jump by words (spaces separate words)
* e                   jump to end of words (punctuation considered words)
* E                   jump to end of words (no punctuation)
* b                   jump backward by words (punctuation considered words)
* B                   jump backward by words (no punctuation)
* ge                  jump backward to end of words
* 0                   (zero) start of line
* ^                   first non-blank character of line
* '$'                 end of line
* '-'                 move line upwards, on the first non blank character
* '+'                 move line downwards, on the first non blank character
* '<enter>'           move line downwards, on the first non blank character
* gg                  go to first line
* G                   go to last line
* nG                  go To line n
* :n                  go To line n
* )                   move the cursor forward to the next sentence.
* (                   move the cursor backward by a sentence.
* {                   move the cursor a paragraph backwards
* }                   move the cursor a paragraph forwards
* ]]                  move the cursor a section forwards or to the next {
* [[                  move the cursor a section backwards or the previous {
* CTRL-f              move the cursor forward by a screen of text
* CTRL-b              move the cursor backward by a screen of text
* CTRL-u              move the cursor up by half a screen
* CTRL-d              move the cursor down by half a screen
* H                   move the cursor to the top of the screen.
* M                   move the cursor to the middle of the screen.
* L                   move the cursor to the bottom of the screen.
* fx                  search line forward for 'x'
* Fx                  search line backward for 'x'
* tx                  search line forward before 'x'
* Tx                  search line backward before 'x'

--------

## BOOKMARKS

* :marks              list all the current marks
* ma                  make a bookmark named a at the current cursor position
* `a                  go to position of bookmark a
* 'a                  go to the line with bookmark a
* `.                  go to the line that you last edited

--------

## INSERT MODE

* i                   start insert mode at cursor
* I                   insert at the beginning of the line
* a                   append after the cursor
* A                   append at the end of the line
* o                   open (append) blank line below current line
* O                   open blank line above current line
* Esc                 exit insert mode

--------

## EDITING

* r                   replace a single character (does not use insert mode)
* R                   enter Insert mode, replacing characters rather than inserting
* J                   join line below to the current one
* cc                  change (replace) an entire line
* cw                  change (replace) to the end of word
* C                   change (replace) to the end of line
* s                   delete character at cursor and substitute text
* S                   delete line at cursor and substitute text (same as cc)
* xp                  transpose two letters (delete and paste, technically)
* u                   undo
* CTRL-r              redo
* .                   repeat last command
* ~                   switch case
* g~iw                switch case of current word
* gUiw                make current word uppercase
* guiw                make current word lowercase
* gU$                 make uppercase until end of line
* gu$                 make lowercase until end of line
* '>>'                Indent line one column to right
* '<<'                Indent line one column to left
* ==                  auto-indent current line
* ddp                 swap current line with next
* ddkp                swap current line with previous
* :%retab             fix spaces / tabs issues in whole file
* :r [name]           insert the file [name] below the cursor.
* :r !{cmd}           execute {cmd} and insert its standard output below the cursor.

--------

## DELETING TEXT

* x                   delete current character
* X                   delete previous character
* dw                  delete the current word
* dd                  delete (cut) a line
* D                   delete from cursor to end of line
* :[range]d           delete [range] lines

--------

## COPYING AND MOVING TEXT

* yw                  yank word
* yy                  yank (copy) a line
* 2yy                 yank 2 lines
* y$                  yank to end of line
* p                   put (paste) the clipboard after cursor/current line
* P                   put (paste) before cursor/current line
* :set paste          avoid unexpected effects in pasting
* :registers          display the contents of all registers
* "xyw                yank word into register x
* "xyy                yank line into register x
* :[range]y x         yank [range] lines into register x
* "xp                 put the text from register x after the cursor
* "xP                 put the text from register x before the cursor
* "xgp                just like "p", but leave the cursor just after the new text
* "xgP                just like "P", but leave the cursor just after the new text
* :[line]put x        put the text from register x after [line]

---

## MACROS

* qa                  start recording macro 'a'
* q                   end recording macro
* @a                  replay macro 'a'
* @:                  replay last command

--------

## VISUAL MODE

* v                   start visual mode, mark lines, then do command (such as y-yank)
* V                   start linewise visual mode
* o                   move to other end of marked area
* U                   upper case of marked area
* CTRL-v              start visual block mode
* O                   move to other corner of block
* aw                  mark a word
* ab                  a () block (with braces)
* ab                  a {} block (with brackets)
* ib                  inner () block
* ib                  inner {} block
* Esc                 exit visual mode

### VISUAL MODE COMMANDS

* '>'                   shift right
* '<'                   shift left
* c                   change (replace) marked text
* y                   yank (copy) marked text
* d                   delete marked text
* ~                   switch case

### VISUAL MODE SHORTCUTS

* v%                  selects matching parenthesis
* vi{                 selects matching curly brace
* vi"                 selects text between double quotes
* vi'                 selects text between single quotes

---

## SPELLING

* ]s                  next misspelled word
* [s                  previous misspelled word
* zg                  add word to wordlist
* zug                 undo last add word
* z=                  suggest word

---


## EXITING


* :q                  quit Vim. This fails when changes have been made.
* :q!                 quit without writing.
* :cq                 quit always, without writing.
* :wq                 write the current file and exit.
* :wq!                write the current file and exit always.
* :wq {file}          write to {file}. Exit if not editing the last
* :wq! {file}         write to {file} and exit always.
* :[range]wq[!]       same as above, but only write the lines in [range].
* ZZ                  write current file, if modified, and exit.
* ZQ                  quit current file and exit (same as ":q!").

--------

## SEARCH/REPLACE

* /pattern                    search for pattern
* ?pattern                    search backward for pattern
* n                           repeat search in same direction
* N                           repeat search in opposite direction
* '*'                         search forward, word under cursor
* '#'                         search backward, word under cursor
* :%s/old/new/g               replace all old with new throughout file
* :%s/old/new/gc              replace all old with new throughout file with confirmation
* :argdo %s/old/new/gc | wq   open multiple files and run this command to replace old with new in every file with confirmation, save and quit

---

## MULTIPLE FILES

* :e filename         edit a file in a new buffer
* :tabe filename      edit a file in a new tab (Vim7, gVim)
* :ls                 list all buffers
* :bn                 go to next buffer
* :bp                 go to previous buffer
* :bd                 delete a buffer (close a file)
* :b1                 show buffer 1
* :b vimrc            show buffer whose filename begins with "vimrc"

--------

## WINDOWS

* :sp f               split open f
* :vsp f              vsplit open f
* CTRL-w s            split windows
* CTRL-w w            switch between windows
* CTRL-w q            quit a window
* CTRL-w v            split windows vertically
* CTRL-w x            swap windows
* CTRL-w h            left window
* CTRL-w j            down window
* CTRL-w k            up window
* CTRL-w l            right window
* CTRL-w +            increase window height
* CTRL-w -            decrease window height
* CTRL-w '<'          increase window width
* CTRL-w '>'          decrease window width
* CTRL-w =            equal window
* CTRL-w o            close other windows

--------

## QUICKFIX WINDOW

* copen               open quickfix window
* cclose              close quickfix window
* cc [nr]             display error [nr]
* cfirst              display the first error
* clast               display the last error
* [count]cn           display [count] next error
* [count]cp           display [count] previous error

--------

## PROGRAMMING

* %                   show matching brace, bracket, or parenthese
* gf                  edit the file whose name is under or after the cursor
* gd                  when the cursor is on a local variable or function, jump to its declaration
* ''                  return to the line where the cursor was before the latest jump
* gi                  return to insert mode where you inserted text the last time
* CTRL-o              move to previous position you were at
* CTRL-i              move to more recent position you were at

--------

## plugins > nerdtree

* :nerdtreetoggle     show / hide file browser
* :nerdtreefind       show current file in file browser
* :bookmark name      bookmark the current node as "name"

### file

* o                   open in prev window
* go                  preview
* t                   open in new tab
* t                   open in new tab silently
* i                   open split
* gi                  preview split
* s                   open vsplit
* gs                  preview vsplit

### directory

* O                   recursively open node
* x                   close parent of node
* X                   close all child nodes of current node recursively
* e                   explore selected dir

### BOOKMARK

* o                   open bookmark
* t                   open in new tab
* T                   open in new tab silently
* D                   delete bookmark

### TREE NAVIGATION

* P                   go to root
* p                   go to parent
* K                   go to first child
* J                   go to last child
* CTRL-j              go to next sibling
* CTRL-k              go to prev sibling

### FILESYSTEM

* C                   change tree root to the selected dir
* u                   move tree root up a dir
* U                   move tree root up a dir but leave old root open
* r                   refresh cursor dir
* R                   refresh current root
* m                   show menu
* cd                  change the CWD to the selected dir

### TREE FILTERING

* I                   hidden files
* f                   file filters
* F                   files
* B                   bookmarks

### OTHER

* q                   close the NERDTree window
* A                   zoom (maximize-minimize) the NERDTree window
* ?                   toggle help

---

## PLUGINS > SURROUND


* cs'"                change surrounding quotes to double-quotes
* cs(}                change surrounding parens to braces
* cs({                change surrounding parens to braces with space
* ds'                 delete surrounding quotes
* dst                 delete surrounding tags
* ysiw[               surround inner word with brackets
* vees'               surround 2 words (ee) with quotes '

--------

## PERSONAL init.vim settings

*leader key is set to SPACE BAR*

### Tab Navigation

* leader t			new tab
* leader h			first tab
* leader j			previous tab
* leader K			next tab
* leader L			last tab

### Splits

* leader =			vertical +10
* leader -			vertical -10
* Ctrl+h,i,j,k,l	Split navigation

### Markdown (Partly plugin based)

* CTRL+m 			Show markdown preview (Live preview, uses web browser &
Plugin)
* leader c			compile markdown into PDF (uses Pandoc)
* leader p 			Show PDF of current document (uses Zathura)

### LATEX (uses Latexlive and pdflatex)

* leader c			Compiles PDF of current TEX document
* leader p			Show PDF of current document (uses Zathura)

### Slide deck (a PDF presentation style document)

* leader o 			Compile current document (md,tex) to a Beamer PDF file.
* leader p 			Show PDF of current document (uses Zathura)
* ,p				Enter title page/theming header (delete theme selection as
	required)

### Plugged

* ALT+p				Plugin install

### NERDtree (Plugin called with Plugged)

* Ctrl+n			NERDTree toggle
* Ctrl+N			NERDTree find

### Goyo (Plugin called with Plugged)

* Ctrl+g			Goyo toggle

### General quality of life stuff

* leader r			source init.vim (get new settings without a reload - Path
* Shift s			Global find and replace (you WILL need chainging for your needs)
* Ctrl+q 			word count
* Ctrl+d			pastes 'this document was created on DATE'
* Ctrl+f			Fzf :Files (called via Plugged)
* Alt+t				Thesaurus (called via Plugged)
* leader space 		clear search criteria
* leader s 			Toggle spell checking
* leader c    	    Compile document (groff/markdown)

#### Defaults

* Plugged plugin manager is installed.
* The Light Bar plugin is installed, this puts a pretty bar at the bottom of
the screen that's slightly better than the default text bar while using very
little resources. Managed by Plugged.
* Line ruler is ON.
* History is set to 100.
* Word Wrap is ON.
* Wild Menu is set to Longest, List, Full.
* Fold is set to manual.
* Auto indent is set to ON.
* Backup files are turned off (you may want to change that if you are new to
VIM).
* Spell checker is set to English GB.
* It's set to NO SHOW MODE because I use Light bar.
There are colour settings for Fzf and spell checking. Background setting is
DARK by default.

### Groff

* leader p			Run Zathura preview (looks for current filename as PDF)
* leader c			Compile groff (save first)

### <++> and code snippets

*there are a bunch of code snippets that are called via , and then a
letter/number. many of these have this <++> thing in them. heres now it works:*

*For example in a markdown file you can call a BOLD (for stars) command with ,b
this will show four stars with your cursor in the middle, and a <++> at the
end. you can type whatever you want made BOLD then ,<tab> (in any mode) to jump
to the <++> and remove it. in the case or more complex code this is a massive
time saver - I found this tip on a youtubers dot files (I think in this case it
was luke smith, who makes great dots, but I know little else about him)*

ALL of these are file type specific

I am not going to list ALL snippets, they can be found by reading the file.
However here is an overview of what's available)

* **Markdown:** all the day to day stuff
* **PHP/HTML:** all the day to day stuff
* **Latex:** I's still learning but I'm pretty sure I got all the standard
stuff.

All of this may have changed since time of writing, more maybe in there.

---

*This was all created with the help of youtubers, blogs, books and strangers
dotfiles*
